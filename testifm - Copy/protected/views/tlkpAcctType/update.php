<?php
/* @var $this TlkpAcctTypeController */
/* @var $model TlkpAcctType */

$this->breadcrumbs=array(
	'Tlkp Acct Types'=>array('index'),
	$model->AcctType=>array('view','id'=>$model->AcctType),
	'Update',
);

$this->menu=array(
	array('label'=>'List TlkpAcctType', 'url'=>array('index')),
	array('label'=>'Create TlkpAcctType', 'url'=>array('create')),
	array('label'=>'View TlkpAcctType', 'url'=>array('view', 'id'=>$model->AcctType)),
	array('label'=>'Manage TlkpAcctType', 'url'=>array('admin')),
);
?>

<h1>Update TlkpAcctType <?php echo $model->AcctType; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>