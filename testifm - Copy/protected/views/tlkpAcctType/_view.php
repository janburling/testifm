<?php
/* @var $this TlkpAcctTypeController */
/* @var $data TlkpAcctType */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('AcctType')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->AcctType), array('view', 'id'=>$data->AcctType)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Description')); ?>:</b>
	<?php echo CHtml::encode($data->Description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PrintSeq')); ?>:</b>
	<?php echo CHtml::encode($data->PrintSeq); ?>
	<br />


</div>