<?php
/* @var $this TlkpAcctTypeController */
/* @var $model TlkpAcctType */

$this->breadcrumbs=array(
	'Tlkp Acct Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TlkpAcctType', 'url'=>array('index')),
	array('label'=>'Manage TlkpAcctType', 'url'=>array('admin')),
);
?>

<h1>Create TlkpAcctType</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>