<?php
/* @var $this TlkpAcctTypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tlkp Acct Types',
);

$this->menu=array(
	array('label'=>'Create TlkpAcctType', 'url'=>array('create')),
	array('label'=>'Manage TlkpAcctType', 'url'=>array('admin')),
);
?>

<h1>Tlkp Acct Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
