<?php
/* @var $this TlkpAcctTypeController */
/* @var $model TlkpAcctType */

$this->breadcrumbs=array(
	'Tlkp Acct Types'=>array('index'),
	$model->AcctType,
);

$this->menu=array(
	array('label'=>'List TlkpAcctType', 'url'=>array('index')),
	array('label'=>'Create TlkpAcctType', 'url'=>array('create')),
	array('label'=>'Update TlkpAcctType', 'url'=>array('update', 'id'=>$model->AcctType)),
	array('label'=>'Delete TlkpAcctType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->AcctType),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TlkpAcctType', 'url'=>array('admin')),
);
?>

<h1>View TlkpAcctType #<?php echo $model->AcctType; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'AcctType',
		'Description',
		'PrintSeq',
	),
)); ?>
