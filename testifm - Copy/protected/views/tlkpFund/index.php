<?php
/* @var $this TlkpFundController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tlkp Funds',
);

$this->menu=array(
	array('label'=>'Create TlkpFund', 'url'=>array('create')),
	array('label'=>'Manage TlkpFund', 'url'=>array('admin')),
);
?>

<h1>Tlkp Funds</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
