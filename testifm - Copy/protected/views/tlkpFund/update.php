<?php
/* @var $this TlkpFundController */
/* @var $model TlkpFund */

$this->breadcrumbs=array(
	'Tlkp Funds'=>array('index'),
	$model->FundCode=>array('view','id'=>$model->FundCode),
	'Update',
);

$this->menu=array(
	array('label'=>'List TlkpFund', 'url'=>array('index')),
	array('label'=>'Create TlkpFund', 'url'=>array('create')),
	array('label'=>'View TlkpFund', 'url'=>array('view', 'id'=>$model->FundCode)),
	array('label'=>'Manage TlkpFund', 'url'=>array('admin')),
);
?>

<h1>Update TlkpFund <?php echo $model->FundCode; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>