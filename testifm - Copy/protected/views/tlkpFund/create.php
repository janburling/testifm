<?php
/* @var $this TlkpFundController */
/* @var $model TlkpFund */

$this->breadcrumbs=array(
	'Tlkp Funds'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TlkpFund', 'url'=>array('index')),
	array('label'=>'Manage TlkpFund', 'url'=>array('admin')),
);
?>

<h1>Create TlkpFund</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>