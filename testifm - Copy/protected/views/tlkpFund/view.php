<?php
/* @var $this TlkpFundController */
/* @var $model TlkpFund */

$this->breadcrumbs=array(
	'Tlkp Funds'=>array('index'),
	$model->FundCode,
);

$this->menu=array(
	array('label'=>'List Fund', 'url'=>array('index')),
	array('label'=>'Create Fund', 'url'=>array('create')),
	array('label'=>'Update Fund', 'url'=>array('update', 'id'=>$model->FundCode)),
	array('label'=>'Delete Fund', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->FundCode),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Fund', 'url'=>array('admin')),
);
?>

<h1>View TlkpFund #<?php echo $model->FundCode; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'RecKey',
		'FundCode',
		'Description',
		'ParentFund',
		'Receipt',
		'Disbursement',
		'AccountsPayable',
		'FundBalance',
		'ApprFundBalance',
		'EstRevenue',
		'Appropriations',
		'Encumbrances',
		'ReserveForEnc',
		'AppropriationExpense',
		'Revenue',
		'InterimFundBalance',
		'DueFromFund',
		'DueToFund',
		'AROverPayment',
		'ARBadDebt',
		'FundType',
		'Status',
		'CreatedDate',
		'ModifiedDate',
		'Who',
	),
)); ?>
