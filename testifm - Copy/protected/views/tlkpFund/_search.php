<?php
/* @var $this TlkpFundController */
/* @var $model TlkpFund */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'RecKey'); ?>
		<?php echo $form->textField($model,'RecKey'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FundCode'); ?>
		<?php echo $form->textField($model,'FundCode',array('size'=>3,'maxlength'=>3)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Description'); ?>
		<?php echo $form->textField($model,'Description',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ParentFund'); ?>
		<?php echo $form->textField($model,'ParentFund',array('size'=>3,'maxlength'=>3)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Receipt'); ?>
		<?php echo $form->textField($model,'Receipt',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Disbursement'); ?>
		<?php echo $form->textField($model,'Disbursement',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'AccountsPayable'); ?>
		<?php echo $form->textField($model,'AccountsPayable',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FundBalance'); ?>
		<?php echo $form->textField($model,'FundBalance',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ApprFundBalance'); ?>
		<?php echo $form->textField($model,'ApprFundBalance',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'EstRevenue'); ?>
		<?php echo $form->textField($model,'EstRevenue',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Appropriations'); ?>
		<?php echo $form->textField($model,'Appropriations',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Encumbrances'); ?>
		<?php echo $form->textField($model,'Encumbrances',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ReserveForEnc'); ?>
		<?php echo $form->textField($model,'ReserveForEnc',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'AppropriationExpense'); ?>
		<?php echo $form->textField($model,'AppropriationExpense',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Revenue'); ?>
		<?php echo $form->textField($model,'Revenue',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'InterimFundBalance'); ?>
		<?php echo $form->textField($model,'InterimFundBalance',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DueFromFund'); ?>
		<?php echo $form->textField($model,'DueFromFund',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'DueToFund'); ?>
		<?php echo $form->textField($model,'DueToFund',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'AROverPayment'); ?>
		<?php echo $form->textField($model,'AROverPayment',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ARBadDebt'); ?>
		<?php echo $form->textField($model,'ARBadDebt',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'FundType'); ?>
		<?php echo $form->textField($model,'FundType',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Status'); ?>
		<?php echo $form->textField($model,'Status',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'CreatedDate'); ?>
		<?php echo $form->textField($model,'CreatedDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ModifiedDate'); ?>
		<?php echo $form->textField($model,'ModifiedDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'Who'); ?>
		<?php echo $form->textField($model,'Who'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->