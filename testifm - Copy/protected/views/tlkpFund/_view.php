<?php
/* @var $this TlkpFundController */
/* @var $data TlkpFund */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('FundCode')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->FundCode), array('view', 'id'=>$data->FundCode)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('RecKey')); ?>:</b>
	<?php echo CHtml::encode($data->RecKey); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Description')); ?>:</b>
	<?php echo CHtml::encode($data->Description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ParentFund')); ?>:</b>
	<?php echo CHtml::encode($data->ParentFund); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Receipt')); ?>:</b>
	<?php echo CHtml::encode($data->Receipt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Disbursement')); ?>:</b>
	<?php echo CHtml::encode($data->Disbursement); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('AccountsPayable')); ?>:</b>
	<?php echo CHtml::encode($data->AccountsPayable); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('FundBalance')); ?>:</b>
	<?php echo CHtml::encode($data->FundBalance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ApprFundBalance')); ?>:</b>
	<?php echo CHtml::encode($data->ApprFundBalance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('EstRevenue')); ?>:</b>
	<?php echo CHtml::encode($data->EstRevenue); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Appropriations')); ?>:</b>
	<?php echo CHtml::encode($data->Appropriations); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Encumbrances')); ?>:</b>
	<?php echo CHtml::encode($data->Encumbrances); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ReserveForEnc')); ?>:</b>
	<?php echo CHtml::encode($data->ReserveForEnc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('AppropriationExpense')); ?>:</b>
	<?php echo CHtml::encode($data->AppropriationExpense); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Revenue')); ?>:</b>
	<?php echo CHtml::encode($data->Revenue); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('InterimFundBalance')); ?>:</b>
	<?php echo CHtml::encode($data->InterimFundBalance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DueFromFund')); ?>:</b>
	<?php echo CHtml::encode($data->DueFromFund); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('DueToFund')); ?>:</b>
	<?php echo CHtml::encode($data->DueToFund); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('AROverPayment')); ?>:</b>
	<?php echo CHtml::encode($data->AROverPayment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ARBadDebt')); ?>:</b>
	<?php echo CHtml::encode($data->ARBadDebt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FundType')); ?>:</b>
	<?php echo CHtml::encode($data->FundType); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Status')); ?>:</b>
	<?php echo CHtml::encode($data->Status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CreatedDate')); ?>:</b>
	<?php echo CHtml::encode($data->CreatedDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ModifiedDate')); ?>:</b>
	<?php echo CHtml::encode($data->ModifiedDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Who')); ?>:</b>
	<?php echo CHtml::encode($data->Who); ?>
	<br />

	*/ ?>

</div>