<?php
/* @var $this TlkpFundController */
/* @var $model TlkpFund */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tlkp-fund-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'FundCode'); ?>
		<?php echo $form->textField($model,'FundCode',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'FundCode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Description'); ?>
		<?php echo $form->textField($model,'Description',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'Description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ParentFund'); ?>
		<?php echo $form->textField($model,'ParentFund',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'ParentFund'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Receipt'); ?>
		<?php echo $form->textField($model,'Receipt',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'Receipt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Disbursement'); ?>
		<?php echo $form->textField($model,'Disbursement',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'Disbursement'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'AccountsPayable'); ?>
		<?php echo $form->textField($model,'AccountsPayable',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'AccountsPayable'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FundBalance'); ?>
		<?php echo $form->textField($model,'FundBalance',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'FundBalance'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ApprFundBalance'); ?>
		<?php echo $form->textField($model,'ApprFundBalance',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'ApprFundBalance'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'EstRevenue'); ?>
		<?php echo $form->textField($model,'EstRevenue',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'EstRevenue'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Appropriations'); ?>
		<?php echo $form->textField($model,'Appropriations',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'Appropriations'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Encumbrances'); ?>
		<?php echo $form->textField($model,'Encumbrances',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'Encumbrances'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ReserveForEnc'); ?>
		<?php echo $form->textField($model,'ReserveForEnc',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'ReserveForEnc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'AppropriationExpense'); ?>
		<?php echo $form->textField($model,'AppropriationExpense',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'AppropriationExpense'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Revenue'); ?>
		<?php echo $form->textField($model,'Revenue',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'Revenue'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'InterimFundBalance'); ?>
		<?php echo $form->textField($model,'InterimFundBalance',array('size'=>25,'maxlength'=>25)); ?>
		<?php echo $form->error($model,'InterimFundBalance'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DueFromFund'); ?>
		<?php echo $form->textField($model,'DueFromFund',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'DueFromFund'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DueToFund'); ?>
		<?php echo $form->textField($model,'DueToFund',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'DueToFund'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'AROverPayment'); ?>
		<?php echo $form->textField($model,'AROverPayment',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'AROverPayment'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ARBadDebt'); ?>
		<?php echo $form->textField($model,'ARBadDebt',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'ARBadDebt'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'FundType'); ?>
		<?php echo $form->textField($model,'FundType',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'FundType'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Status'); ?>
		<?php echo $form->textField($model,'Status',array('size'=>1,'maxlength'=>1)); ?>
		<?php echo $form->error($model,'Status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'CreatedDate'); ?>
		<?php echo $form->textField($model,'CreatedDate'); ?>
		<?php echo $form->error($model,'CreatedDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ModifiedDate'); ?>
		<?php echo $form->textField($model,'ModifiedDate'); ?>
		<?php echo $form->error($model,'ModifiedDate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Who'); ?>
		<?php echo $form->textField($model,'Who'); ?>
		<?php echo $form->error($model,'Who'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
