<?php

/**
 * This is the model class for table "tlkpFund".
 *
 * The followings are the available columns in table 'tlkpFund':
 * @property integer $RecKey
 * @property string $FundCode
 * @property string $Description
 * @property string $ParentFund
 * @property string $Receipt
 * @property string $Disbursement
 * @property string $AccountsPayable
 * @property string $FundBalance
 * @property string $ApprFundBalance
 * @property string $EstRevenue
 * @property string $Appropriations
 * @property string $Encumbrances
 * @property string $ReserveForEnc
 * @property string $AppropriationExpense
 * @property string $Revenue
 * @property string $InterimFundBalance
 * @property string $DueFromFund
 * @property string $DueToFund
 * @property string $AROverPayment
 * @property string $ARBadDebt
 * @property string $FundType
 * @property string $Status
 * @property string $CreatedDate
 * @property string $ModifiedDate
 * @property integer $Who
 */
class TlkpFund extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tlkpFund';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('FundCode, DueFromFund, DueToFund, AROverPayment, ARBadDebt', 'required'),
			array('Who', 'numerical', 'integerOnly'=>true),
			array('FundCode, ParentFund', 'length', 'max'=>3),
			array('Description', 'length', 'max'=>50),
			array('Receipt, Disbursement, AccountsPayable, FundBalance, ApprFundBalance, EstRevenue, Appropriations, Encumbrances, ReserveForEnc, AppropriationExpense, Revenue, InterimFundBalance', 'length', 'max'=>25),
			array('DueFromFund, DueToFund, AROverPayment, ARBadDebt', 'length', 'max'=>30),
			array('FundType, Status', 'length', 'max'=>1),
			array('CreatedDate, ModifiedDate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('RecKey, FundCode, Description, ParentFund, Receipt, Disbursement, AccountsPayable, FundBalance, ApprFundBalance, EstRevenue, Appropriations, Encumbrances, ReserveForEnc, AppropriationExpense, Revenue, InterimFundBalance, DueFromFund, DueToFund, AROverPayment, ARBadDebt, FundType, Status, CreatedDate, ModifiedDate, Who', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'RecKey' => 'Rec Key',
			'FundCode' => 'Fund Code',
			'Description' => 'Description',
			'ParentFund' => 'Parent Fund',
			'Receipt' => 'Receipt',
			'Disbursement' => 'Disbursement',
			'AccountsPayable' => 'Accounts Payable',
			'FundBalance' => 'Fund Balance',
			'ApprFundBalance' => 'Appr Fund Balance',
			'EstRevenue' => 'Est Revenue',
			'Appropriations' => 'Appropriations',
			'Encumbrances' => 'Encumbrances',
			'ReserveForEnc' => 'Reserve For Enc',
			'AppropriationExpense' => 'Appropriation Expense',
			'Revenue' => 'Revenue',
			'InterimFundBalance' => 'Interim Fund Balance',
			'DueFromFund' => 'Due From Fund',
			'DueToFund' => 'Due To Fund',
			'AROverPayment' => 'Arover Payment',
			'ARBadDebt' => 'Arbad Debt',
			'FundType' => 'Fund Type',
			'Status' => 'Status',
			'CreatedDate' => 'Created Date',
			'ModifiedDate' => 'Modified Date',
			'Who' => 'Who',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('RecKey',$this->RecKey);
		$criteria->compare('FundCode',$this->FundCode,true);
		$criteria->compare('Description',$this->Description,true);
		$criteria->compare('ParentFund',$this->ParentFund,true);
		$criteria->compare('Receipt',$this->Receipt,true);
		$criteria->compare('Disbursement',$this->Disbursement,true);
		$criteria->compare('AccountsPayable',$this->AccountsPayable,true);
		$criteria->compare('FundBalance',$this->FundBalance,true);
		$criteria->compare('ApprFundBalance',$this->ApprFundBalance,true);
		$criteria->compare('EstRevenue',$this->EstRevenue,true);
		$criteria->compare('Appropriations',$this->Appropriations,true);
		$criteria->compare('Encumbrances',$this->Encumbrances,true);
		$criteria->compare('ReserveForEnc',$this->ReserveForEnc,true);
		$criteria->compare('AppropriationExpense',$this->AppropriationExpense,true);
		$criteria->compare('Revenue',$this->Revenue,true);
		$criteria->compare('InterimFundBalance',$this->InterimFundBalance,true);
		$criteria->compare('DueFromFund',$this->DueFromFund,true);
		$criteria->compare('DueToFund',$this->DueToFund,true);
		$criteria->compare('AROverPayment',$this->AROverPayment,true);
		$criteria->compare('ARBadDebt',$this->ARBadDebt,true);
		$criteria->compare('FundType',$this->FundType,true);
		$criteria->compare('Status',$this->Status,true);
		$criteria->compare('CreatedDate',$this->CreatedDate,true);
		$criteria->compare('ModifiedDate',$this->ModifiedDate,true);
		$criteria->compare('Who',$this->Who);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TlkpFund the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
